#include <stdio.h>
#include <stdlib.h>
#include <canna/jrkanji.h>

#define _jrKanjiControl(a, b, c) \
	printf("%d\n", b); \
	jrKanjiControl(a, b, c);


void
do_init(int context_id) {
  char* workbuf;
  jrKanjiStatusWithValue ksv;
  jrKanjiStatus ks;

  workbuf = calloc(sizeof(char), 1024);

  ksv.ks = &ks;
  ksv.val = CANNA_MODE_HenkanMode;
  ksv.buffer = workbuf;
  ksv.bytes_buffer = 1024;

  _jrKanjiControl(context_id, KC_CHANGEMODE, (void*)&ksv);
}

int main() {
  char modebuf[1024];
  int len;

  _jrKanjiControl(0, KC_INITIALIZE, 0);
  do_init(0);
  _jrKanjiControl(1, KC_INITIALIZE, 0);
  do_init(1);
  _jrKanjiControl(2, KC_INITIALIZE, 0);
  do_init(2);
  _jrKanjiControl(0, KC_FINALIZE, 0);
  do_init(0);
  len = _jrKanjiControl(0, KC_QUERYMAXMODESTR, 0);
  _jrKanjiControl(0, KC_QUERYMODE, modebuf);
  printf("\"%s\"\n", modebuf);
  return 0;
}
