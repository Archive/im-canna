/* GtkEntry test
 */

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

int
key_pressed(GtkWidget* widget, GdkEventKey* event) {
  if( event->keyval == GDK_Escape ) {
    gtk_main_quit();
  }

  return FALSE;
}

int main(int argc, char** argv) {
  GtkWidget* window;
  GtkWidget* entry;

  gtk_set_locale();
  gtk_init(&argc, &argv);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  
  entry = gtk_entry_new();
  gtk_container_add(GTK_CONTAINER(window), entry);

  gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
  g_signal_connect(G_OBJECT(window), "key_press_event", G_CALLBACK(key_pressed), NULL);

  gtk_widget_show_all(window);

  gtk_main();

  return 0;
}
