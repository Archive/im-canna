#!/usr/bin/python

import sys,httplib,gzip,codecs,cStringIO

h = httplib.HTTP('pc8.2ch.net')
h.putrequest('GET', '/linux/dat/1118609841.dat')
#h.putheader('Cache-Control', 'max-age=0')
h.putheader('Accept', '*/*')
h.putheader('Accept-Charset', 'Shift_JIS,utf-8;q=0.7,*;q=0.7')
h.putheader('Accept-Language', 'ja')
h.putheader('User-Agent', 'Monazilla/1.00 (Monaic-gDesklets/1.0)')
h.putheader('Accept-Encoding', 'gzip,deflate')
h.putheader('Connection', 'close')
h.putheader('Host', 'pc8.2ch.net')
h.putheader('Cookie', 'PON=221.218.38.181; MAIL=; NAME=; HAP=1043030')
h.putheader('Keep-Alive', '300')
h.putheader('Proxy-Connection', 'keep-alive')
h.endheaders()
errcode, errmsg, headers = h.getreply()
#print errcode # Should be 200
f = h.getfile()
data = f.read() # Get the raw HTML
f.close()

f = cStringIO.StringIO(data)

g = gzip.GzipFile("2ch-cache.gz", "rb", 6, f)
g = codecs.EncodedFile(g, "UTF-8", "cp932")
print g.read()

