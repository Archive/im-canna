from libdesklets.controls import Interface, Permission

class IHttp(Interface):

    # properties along with their permissions
    url = Permission.READWRITE
    error = Permission.READ
    status = Permission.READ
    percent = Permission.READ
