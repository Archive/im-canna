from libdesklets.controls import Control

from IHttp import IHttp

import os
import sys,httplib,gzip,codecs,cStringIO,string
import socket
import threading
import time
import syslog

class download(threading.Thread):
    def __init__(self, url):
      threading.Thread.__init__(self)
      self.__dict = dict()
      self.__dict['url'] = url
      self.__dict['status'] = "start"

    def run(self) :
        for i in range(0, 20) :
            self.__dict['status'] = "thread: " + str(i)
            time.sleep(1)
        self.__dict['status'] = "END"

    def get_status(self):
        return self.__dict['status']

#
# Control for Http url visual downloading.
#
class Http(Control, IHttp):

    def __init__(self):

        Control.__init__(self)

        self.__url = ""
        self.__timer_id = 0
        self.__status = 0
        self.__percent = 0
        self.__download = download("")

    def __update_status(self):
        self._update("status")
        return True

    def __get_url(self): return self.__url

    def __set_url(self, url):
        self.__url = url
        if self.__timer_id != 0:
          self.__remove_timer(self.__timer_id)
        self.__timer_id = self._add_timer(1000, self.__update_status)
        self.__status = "Set URL"
        self.__download = download(url)
        self.__download.start()

    def __get_error(self): return "0"

    def __get_status(self):
        self.__status = self.__download.get_status()
        if self.__download.isAlive():
          self.__status += "." * (int(time.time()) % 4 + 1)
        return self.__status

    def __get_percent(self):
        self.__percent += 1
        return str(self.__percent)

    def __get_http(self, url, proto):
      elems = url.split("/")
      host = elems[2]
      elems[0:3] = []
      uri = "/" + string.join(elems, "/")

      h = httplib.HTTP(host)
      h.putrequest('GET', uri)
      #h.putheader('Cache-Control', 'max-age=0')
      h.putheader('Accept', '*/*')
      h.putheader('Accept-Charset', 'Shift_JIS,utf-8;q=0.7,*;q=0.7')
      h.putheader('Accept-Language', 'ja')
      h.putheader('User-Agent', 'Monazilla/1.00 (Monaic-gDesklets/1.0)')
      h.putheader('Accept-Encoding', 'gzip,deflate')
      h.putheader('Connection', 'close')
      h.putheader('Host', host)
      h.putheader('Keep-Alive', '300')
      h.putheader('Proxy-Connection', 'keep-alive')
      h.endheaders()
      return h

    def __get_http_head(self, url):
      h = self.__get_http(url, 'HEAD')
      errcode, errmsg, headers = h.getreply()
      #print errcode # Should be 200
      return headers

    def __get_http_get(self, url):
      h = self.__get_http(url, 'GET')
      self.__status = "Downloading"
      errcode, errmsg, headers = h.getreply()
      #print errcode # Should be 200
      f = h.getfile()
      data = f.read() # Get the raw HTML
      f.close()
      f = cStringIO.StringIO(data)
      # Gzip handling
      if "Content-Encoding" in headers:
        if headers["Content-Encoding"].find("gzip") != -1:
          g = gzip.GzipFile("2ch-cache.gz", "rb", 6, f)
          g = codecs.EncodedFile(g, "UTF-8", "cp932")
          return g.read()
        else:
          return data
      else:
        f = codecs.EncodedFile(f, "UTF-8", "cp932")
        return f.read()

    url     = property(__get_url, __set_url, doc = "url")
    error   = property(__get_error, doc = "error")
    status  = property(__get_status, doc = "status")
    percent = property(__get_percent, doc = "percent")

def get_class(): return Http
