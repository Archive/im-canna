from libdesklets.controls import Control

from Inich import Inich

import time
import os
import re
import string
import sys,httplib,gzip,codecs,string,cStringIO

#
# Control for time-related stuff.
#
class Nich(Control, Inich):

    def __init__(self):

        self.__index = 0
        self.__category = 0
        self.__ita = 0
        self.__sure = 0
        self.__sureuris = []

        Control.__init__(self)

        # Making category lists
        category = open("/home/nakai/2ch/categorylist", "rb")
        cats = category.read()
        category.close()
        categories = re.findall('\xe3\x80\x90<B>(.*)</B>\xe3\x80\x91', cats)
        # Cut non-2ch categories
        for i in range(len(categories)):
          if categories[i] == "大人の時間":
            categories[i:len(categories)] = []
            break
        self.__categories = categories

        # Making ita lists
        elems = re.split('\xe3\x80\x90<B>.*</B>\xe3\x80\x91', cats)

        c2ita = {}
        ita2url = {}
        for i in range(len(self.__categories)):
          urllist = re.findall('<A HREF=.*>.*</A>', elems[i+1])
          c2ita[self.__categories[i]] = []
          for j in range(len(urllist)):
            mo = re.search('<A HREF=(.*)>(.*)</A>', urllist[j])
            itaname  =  mo.string[mo.start(2):mo.end(2)]
            itaurl =  mo.string[mo.start(1):mo.end(1)]
            itaurl = string.replace(itaurl, " TARGET=_", "")
            c2ita[self.__categories[i]].append(itaname)
            ita2url[itaname] = itaurl

        self.__c2ita = c2ita
        self.__ita2url = ita2url
       
        # Making sure lists
        sure = open("/home/nakai/2ch/out", "rb")
        lines = sure.readlines()
        sure.close()

        for i in range(len(lines)):
          j = len(lines) - i - 1
          if lines[j].find('<>') == -1:
            del lines[j]

        self.__lines = lines
        self.__maxlines = len(self.__lines)

        # Formatting name,name,article...
        self.__articles = []
        for i in range(len(lines)):
          elems = self.__lines[i].split('<>')
          name = elems[0]
          email = elems[1]
          date = elems[2]
          article = elems[3]

          # Formatting name...
          if email != "":
            name = "<span foreground=\"blue\"><u>" + name + "</u></span>"
          else:
            name = "<span foreground=\"darkgreen\">" + name + "</span>"
          name = str(i + 1) + " " + name + "   "

          # Formatting article...
          article = string.replace(article, "&gt;", "&#x003e;")
          article = string.replace(article, "&lt;", "&#x003c;")
          article = string.replace(article, "=", "&#x003d;")
          article = re.sub(r'&([^#])', r'&#x0026;\1', article)
          article = string.replace(article, "<br>", "\n")
          article = string.replace(article, "</a>", "</u></span>")
          article = re.sub(r'<a[^>]*>', "<span foreground=\"blue\"><u>", article)
          linecount = article.count("\n")
          if linecount < 5:
            article += "\n" * (5 - linecount)
          self.__articles.append(name + date + "\n" + article)

    def __get_index(self):

        return self.__index

    def __set_index(self, index):

        if index < self.__maxlines:
          self.__index = index
        else:
          self.__index = self.__maxlines - 1

    def __get_article(self):

        return self.__articles[self.__index]

    def __get_catmenu(self):

        return self.__categories

    def __get_itamenu(self):

        catname = self.__categories[self.__category]
        return self.__c2ita[catname]

    def __get_category(self):

        return 0

    def __set_category(self, category):

        self.__category = category

    def __get_ita(self):

        return self.__ita

    def __set_ita(self, ita):

        self.__ita = ita

    def __get_suremenu(self):

        catname = self.__categories[self.__category]
        itamenu = self.__c2ita[catname]
        itaname = itamenu[self.__ita]
        sbjurl = self.__ita2url[itaname] + "subject.txt"
        subject = get_url(sbjurl) 
        f = cStringIO.StringIO(subject)
        g = gzip.GzipFile("2ch-cache.gz", "rb", 6, f)
        g = codecs.EncodedFile(g, "UTF-8", "cp932")
        datlist = g.readlines()
        g.close()

        suremenu = []
        sureuris = []
        for i in range(len(datlist)):
          datinfo = datlist[i].split('<>')
          sureuri = datinfo[0]
          suretitle = string.replace(datinfo[1], "\n", "")
          suremenu.append(suretitle)
          sureuris.append(sureuri)

        self.__sureuris = sureuris
        return suremenu

    def __get_sure(self):

        return self.__sure

    def __set_sure(self, sure):

        self.__sure = sure

        catname = self.__categories[self.__category]
        itamenu = self.__c2ita[catname]
        itaname = itamenu[self.__ita]
        sureuri = self.__sureuris[self.__sure]
        daturl = self.__ita2url[itaname] + "dat/" + sureuri
        datdata = get_url(daturl)
        f = cStringIO.StringIO(datdata)
        g = gzip.GzipFile("2ch-cache.gz", "rb", 6, f)
        g = codecs.EncodedFile(g, "UTF-8", "cp932")
        lines = g.readlines()
        g.close()
        self.__lines = lines
        self.__maxlines = len(self.__lines)

        # Formatting name,name,article...
        self.__articles = []
        for i in range(len(lines)):
          elems = self.__lines[i].split('<>')
          name = elems[0]
          email = elems[1]
          date = elems[2]
          article = elems[3]

          # Formatting name...
          if email != "":
            name = "<span foreground=\"blue\"><u>" + name + "</u></span>"
          else:
            name = "<span foreground=\"darkgreen\">" + name + "</span>"
          name = str(i + 1) + " " + name + "   "

          # Formatting article...
          article = string.replace(article, "&gt;", "&#x003e;")
          article = string.replace(article, "&lt;", "&#x003c;")
          article = string.replace(article, "=", "&#x003d;")
          article = re.sub(r'&([^#])', r'&#x0026;\1', article)
          article = string.replace(article, "<br>", "\n")
          article = string.replace(article, "</a>", "</u></span>")
          article = re.sub(r'<a[^>]*>', "<span foreground=\"blue\"><u>", article)
          linecount = article.count("\n")
          if linecount < 5:
            article += "\n" * (5 - linecount)
          self.__articles.append(name + date + "\n" + article)
        
        
    index    = property(__get_index, __set_index, doc = "index")
    article  = property(__get_article, doc = "article")
    catmenu  = property(__get_catmenu, doc = "Category menu")
    itamenu  = property(__get_itamenu, doc = "Ita menu")
    suremenu = property(__get_suremenu, doc = "Sure menu")
    category = property(__get_category, __set_category, doc = "Category index")
    ita      = property(__get_ita, __set_ita, doc = "Ita index")
    sure     = property(__get_sure, __set_sure, doc = "Sure index")

def get_class(): return Nich

def get_url(url):
  list = url.split('/')
  host = list[2]
  list[0:3] = []
  uri = '/' + string.join(list, '/')
  h = httplib.HTTP(host)
  h.putrequest('GET', uri)
  #h.putheader('Cache-Control', 'max-age=0')
  h.putheader('Accept', '*/*')
  h.putheader('Accept-Charset', 'Shift_JIS,utf-8;q=0.7,*;q=0.7')
  h.putheader('Accept-Language', 'ja')
  h.putheader('User-Agent', 'Monazilla/1.00 (Monaic-gDesklets/1.0)')
  h.putheader('Accept-Encoding', 'gzip,deflate')
  h.putheader('Connection', 'close')
  h.putheader('Host', host)
  h.putheader('Cookie', 'PON=221.218.38.181; MAIL=; NAME=; HAP=1043030')
  h.putheader('Keep-Alive', '300')
  h.putheader('Proxy-Connection', 'keep-alive')
  h.endheaders()
  errcode, errmsg, headers = h.getreply()
  f = h.getfile()
  data = f.read() # Get the raw HTML
  f.close()

  return data

