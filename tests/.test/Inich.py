from libdesklets.controls import Interface, Permission

class Inich(Interface):

    # properties along with their permissions
    index = Permission.READWRITE
    article = Permission.READ
    catmenu = Permission.READ
    itamenu = Permission.READ
    suremenu = Permission.READ
    category = Permission.READWRITE
    ita = Permission.READWRITE
    sure = Permission.READWRITE
