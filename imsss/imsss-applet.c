/* 
 * IMSSS Applet
 *
 * Copyright (C) 2003 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <libbonoboui.h>
#include <gtk/gtktooltips.h>
#include <panel-applet.h>
#include <libgnomeui/libgnomeui.h>

#define BUFSIZE 1024

#define IMSSS_STATUS_KANA           "STATUS KANA"
#define IMSSS_STATUS_KANJI          "STATUS KANJI"
#define IMSSS_STATUS_NONE           "STATUS NONE"

#define IMSSS_SOCKET "/tmp/.imsss"

#define PACKAGE "imsss-applet"
#define VERSION "0.0"

static GtkTooltips *imsss_applet_tooltips = NULL;

static void
imsss_applet_about (BonoboUIComponent *uic,
		   gpointer           user_data,
		   const gchar       *verbname)
{
	GtkWidget* imsss_about = NULL;

	const gchar *authors[] = { "Yukihiro Nakai <nakai@gnome.gr.jp>", NULL };

	const gchar *documenters[] = { NULL };

	imsss_about = gnome_about_new(PACKAGE, VERSION,
                                "Copyright \xc2\xa9 2003 Yukihiro Nakai",
                                "IMSSS Applet",
                                authors,
                                documenters,
                                "translator_credits",
                                NULL);
	gtk_widget_show(imsss_about);
}

static const BonoboUIVerb imsss_applet_menu_verbs [] = {
        BONOBO_UI_VERB ("IMSSSAbout", imsss_applet_about),

        BONOBO_UI_VERB_END
};

static const char imsss_applet_menu_xml [] =
	"<popup name=\"button3\">\n"
	"   <menuitem name=\"IMSSS About\" verb=\"IMSSSAbout\" _label=\"About...\" pixtype=\"stock\" pixname=\"gnome-stock-about\"/>\n"
	"</popup>\n";

typedef struct {
	PanelApplet   base;
	GtkWidget    *button;
	GtkWidget    *label;
	int fd1, fd2;
} IMSSSApplet;

static GType
imsss_applet_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (PanelAppletClass),
			NULL, NULL, NULL, NULL, NULL,
			sizeof (IMSSSApplet),
			0, NULL, NULL
		};

		type = g_type_register_static (
				PANEL_TYPE_APPLET, "IMSSSApplet", &info, 0);
	}

	return type;
}

static void
input_func(gpointer data, gint source, GdkInputCondition cond) {
  gchar buf[BUFSIZE];
  IMSSSApplet* applet = (IMSSSApplet*)data;
  int len;
  int ret;
  struct sockaddr_un caddr;

  applet->fd2 = accept(applet->fd1, (struct sockaddr *)&caddr, &len);
  if( applet->fd2 < 0 ) {
    perror("accept");
    return;
  }

  ret = read(applet->fd2, buf, BUFSIZE);
  close(applet->fd2);
  if( !strncmp(buf, IMSSS_STATUS_KANA, strlen(IMSSS_STATUS_KANA)) )  {
    gtk_widget_set_sensitive(applet->button, TRUE);
    gtk_label_set_markup(GTK_LABEL(applet->label),
			 "<span size=\"xx-large\">あ</span>");
  } else if( !strncmp(buf, IMSSS_STATUS_KANJI, strlen(IMSSS_STATUS_KANJI)) ) {
    gtk_widget_set_sensitive(applet->button, TRUE);
    gtk_label_set_markup(GTK_LABEL(applet->label),
			 "<span size=\"xx-large\">漢</span>");
  } else if( !strncmp(buf, IMSSS_STATUS_NONE, strlen(IMSSS_STATUS_NONE)) ) {
    gtk_widget_set_sensitive(applet->button, FALSE);
    gtk_label_set_markup(GTK_LABEL(applet->label),
			 "<span size=\"xx-large\">あ</span>");
  }
}


static void
imsss_applet_setup_tooltips (GtkWidget *widget)
{
	g_return_if_fail (!GTK_WIDGET_NO_WINDOW (widget));

	if (!imsss_applet_tooltips)
		imsss_applet_tooltips = gtk_tooltips_new ();

	gtk_tooltips_set_tip (imsss_applet_tooltips, widget, "IMSSS Applet", "");
}

static gboolean
imsss_applet_fill (IMSSSApplet *applet)
{
	GtkWidget* hbox;

	struct sockaddr_un saddr;
	if( (applet->fd1 = socket(PF_UNIX, SOCK_STREAM, 0)) < 0 ) {
		perror("socket");
		return FALSE;
	}
	bzero((char*)&saddr, sizeof(saddr));
	saddr.sun_family = AF_UNIX;
	strcpy(saddr.sun_path, IMSSS_SOCKET);
	unlink(IMSSS_SOCKET);
	if( bind(applet->fd1, (struct sockaddr*)&saddr,
		sizeof(saddr.sun_family)+strlen(IMSSS_SOCKET)) < 0 ) {
		perror("bind");
		return FALSE;
	}
	if( listen(applet->fd1, 1) < 0 ) {
		perror("listen");
		return FALSE;
	}

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(applet), hbox);

	applet->button = gtk_button_new_with_label("あ");
	applet->label = GTK_BIN(applet->button)->child;
	gtk_widget_set_sensitive(applet->button, FALSE);

	gtk_box_pack_start(GTK_BOX(hbox), applet->button, TRUE, TRUE, 0);

	GTK_WIDGET_UNSET_FLAGS (applet->button, GTK_CAN_FOCUS);
	GTK_WIDGET_UNSET_FLAGS (applet, GTK_CAN_FOCUS);

	gtk_label_set_markup(GTK_LABEL(applet->label),
			"<span size=\"xx-large\">あ</span>");

	gtk_widget_show_all (GTK_WIDGET (applet));

	panel_applet_setup_menu (
		PANEL_APPLET (applet), imsss_applet_menu_xml, imsss_applet_menu_verbs, NULL);

	imsss_applet_setup_tooltips (GTK_WIDGET (applet));

	panel_applet_set_flags (PANEL_APPLET (applet), PANEL_APPLET_HAS_HANDLE);

	gdk_input_add_full(applet->fd1, GDK_INPUT_READ,
			   input_func, applet, NULL);

	return TRUE;
}

static gboolean
imsss_applet_factory (IMSSSApplet  *applet,
		     const gchar *iid,
		     gpointer     data)
{
	gboolean retval = FALSE;
    
	if (!strcmp (iid, "OAFIID:GNOME_Panel_IMSSSBonoboApplet"))
		retval = imsss_applet_fill (applet); 
    
	return retval;
}

PANEL_APPLET_BONOBO_FACTORY ("OAFIID:GNOME_Panel_IMSSSBonoboApplet_Factory",
			     imsss_applet_get_type (),
			     "IM Status Sharing Applet",
			     "0",
			     (PanelAppletFactoryCallback) imsss_applet_factory,
			     NULL)
