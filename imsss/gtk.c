/* 
 * Copyright (C) 2003 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <gtk/gtk.h>

#define BUFSIZE 1024

#define IMSSS_STATUS_KANA           "STATUS KANA"
#define IMSSS_STATUS_KANJI          "STATUS KANJI"
#define IMSSS_STATUS_NONE           "STATUS NONE"

#define IMSSS_SOCKET                "/tmp/.imsss"

int fd1, fd2;
GtkWidget* button;

void socket_init();
void input_func(gpointer data, gint source, GdkInputCondition cond);

void
input_func(gpointer data, gint source, GdkInputCondition cond) {
  gchar buf[BUFSIZE];
  int len;
  int ret;
  struct sockaddr_un caddr;

  fd2 = accept(fd1, (struct sockaddr *)&caddr, &len);
  if( fd2 < 0 ) {
    perror("accept");
    close(fd1);
    close(fd2);
    socket_init();
    return;
  }

  ret = read(fd2, buf, BUFSIZE);
  close(fd2);
  if( !strncmp(buf, IMSSS_STATUS_KANA, strlen(IMSSS_STATUS_KANA)) )  {
    gtk_widget_set_sensitive(button, TRUE);
    gtk_button_set_label(GTK_BUTTON(button), "あ");
  } else if( !strncmp(buf, IMSSS_STATUS_KANJI, strlen(IMSSS_STATUS_KANJI)) ) {
    gtk_widget_set_sensitive(button, TRUE);
    gtk_button_set_label(GTK_BUTTON(button), "漢");
  } else if( !strncmp(buf, IMSSS_STATUS_NONE, strlen(IMSSS_STATUS_NONE)) ) {
    gtk_widget_set_sensitive(button, FALSE);
    gtk_button_set_label(GTK_BUTTON(button), "あ");
  }

}

void
socket_init() {
  struct sockaddr_un saddr;

  if ((fd1 = socket(PF_UNIX, SOCK_STREAM, 0)) < 0 ) {
    perror("socket");
    exit(1);
  }

  bzero((char*)&saddr, sizeof(saddr));

  saddr.sun_family = AF_UNIX;
  strcpy(saddr.sun_path, IMSSS_SOCKET);
  unlink(IMSSS_SOCKET);
  if( bind(fd1, (struct sockaddr*)&saddr,
	   sizeof(saddr.sun_family) + strlen(IMSSS_SOCKET)) < 0 ) {
    perror("bind");
    exit(1);
  }

  if( listen(fd1, 1) < 0 ) {
    perror("listen");
    exit(1);
  }

  gdk_input_add_full(fd1, GDK_INPUT_READ, input_func, NULL, NULL);

}

int main(int argc, char** argv) {
  GtkWidget *window;

  gtk_set_locale();
  gtk_init(&argc, &argv);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  button = gtk_button_new_with_label("あ");
  gtk_widget_set_sensitive(button, FALSE);
  GTK_WIDGET_UNSET_FLAGS (button, GTK_CAN_FOCUS);

  gtk_container_add(GTK_CONTAINER(window), button);

  gtk_widget_show_all(window);

  socket_init();

  gtk_main();
}
